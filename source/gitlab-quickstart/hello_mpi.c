#define _GNU_SOURCE

#include <utmpx.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "mpi.h"

int main (int argc, char *argv[]) {
  int           my_rank=0;
  int           sz;
  int sched_getcpu();
  int cpu;
  char hostname[MPI_MAX_PROCESSOR_NAME]="UNKNOWN";

  cpu = sched_getcpu();

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Get_processor_name(hostname, &sz);

  printf("Hello, from host %s from CPU %d  rank %d\n", hostname, cpu, my_rank);

  if (argc > 1)
    sleep (atoi(argv[1]));

  MPI_Finalize();
  
  return 0;
}
