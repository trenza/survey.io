Welcome to the *survey* tool documentation
==========================================

**The survey performance collector**

.. image:: docs/files/survey_logo_tmp.jpg
  :scale: 6%
  :align: right

The *survey* collector/analytics framework is a new generation, high-level,
light-weight multi-platform Linux tool set that targets metric collection for
high-level performance analysis of applications running on both single node
and large-scale Power and x86_64 platforms, including the Cray platforms.

The  collector is designed to work on sequential, MPI, OpenMP, and hybrid
codes and directly leverages several interfaces available for tools inside
current MPI implementations including: MPICH, MVAPICH, MPT, and OpenMPI.  It
also supports multiple architectures and has been tested on machines based on
Intel, AMD, ARM, and IBM P8/9 processors and integrated GPUs.

The *survey* collector features include:

  * *Lightweight measurements with a target goal of 1 % overhead*
  * *Gathers multiple performance metrics in one run*
  * *High level performance overview with no mapping back to the source*
  * *Identification of potential areas of further investigation with a more
    detailed tool such as Open|SpeedShop or HPCToolkit.*
  * *Summary report data to standard output as well as csv and json-formatted
    files*
  * *Raw per-rank/per-thread measurement data in csv format*
  * *Min, Max, Average output across all ranks and threads of execution*
  * *Ability to produce Min, Max, Average output across only selected ranks and
    threads of execution*
  * *Intel TMA top-down analysis across all or selected ranks and threads of
    execution*
  * *Metadata information on the system just before the run*

*survey* performance metrics gathered include:

  * *Metrics for all MPI ranks and OMP thread*
  * *Memory information, such as high water mark, memory allocation and free
    calls, allocation sizes*
  * *Hardware counter information and derived metrics*
  * *User-defined PAPI or native-PAPI counters*
  * *Input/output information, I/O time, read / write times and byte counts*
  * *MPI information, MPI time and percent across the threads of execution*
  * *OpenMP information, serial time and time spent in OpenMP regions*
  * *Kokkos information, kernel information including counts and time spent in kernels*
  * *Summary job metadata including system information before the run*
  * *Affinity detail regarding resource used*
  * *numa configuration detail*

The *survey* collector targets multiple use cases.

  * Developers can use  to assess how their application performs on
    different machines through the development process.
  * System administrators can incorporate  performance indicators into
    testing infrastructure tools to assess impacts of changes to the system or
    software environments.
  * Development teams can incorporate into  data into their Continuous
    Integration (CI) process to track and compare performance across system
    architectures.


.. toctree::
   :caption: Contents
   :maxdepth: 2

   docs/introduction.rst
   docs/videos.rst
   docs/component.rst
   docs/running.rst
   docs/collector.rst
   docs/data.rst
   docs/analysis.rst
   docs/project.rst
   docs/log.rst
   docs/database.rst
   docs/plot.rst
   docs/report.rst
   docs/viewer.rst
   docs/compare.rst
   docs/extractor.rst
   docs/outliers.rst
   docs/plugins.rst
   docs/advanced.rst
   docs/top_down.rst
   docs/installing.rst
