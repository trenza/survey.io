Run-time Collector
==================

These are the measurements that are collected during a *Survey* run per rank/thread:

Summary
-------

* Total time (s)

DMEM
----
Collected from papi_dmem_info

* DMEM size (kB)
* DMEM resident (kB)
* DMEM high water mark (kB)
* DMEM shared (kB)
* DMEM heap (kB)

MEMALLOC
--------
This collector is included with the *survey* --enable-mem option.

* Allocation time (s)
* Allocation calls
* Allocation bytes

MEMFREE
-------
This collector is included with the *survey* --enable-mem option.

* Free time (s)
* Free calls

RUSAGE
------

* MaxRSS (KiB)
* CPU user time (s)
* CPU system time (s)
* Minflt
* Maxflt
* outblock
* nvcsw
* nivcsw

IO
--

* io total time (s)
* read time (s)
* write time (s)
* read bytes
* write bytes
* io percentage of total time

OMPT
----

* implicit_task_time_seconds
* serial_time_seconds
* barrier_time_seconds
* omp_computer (implicit_task_time_seconds - serial_time_seconds - barrier_time_seconds)

MPI
---

* total time (s)
* mpi percentage of total time


PAPI
----
This is a partial list of available counters. Custom counters are also available:

* PAPI_TOT_CYC
* PAPI_TOT_INS
* PAPI_LD_INS
* PAPI_VEC_DP
* PAPI_DP_OPS

Derived PAPI Calculations
-------------------------
This is a partial list of derived measurements. Additional calculations are included if the needed counters have been measured and have non-zero values.

* Computational intensity: PAPI_TOT_CYC / PAPI_TOT_INS
* Ratio of load instruc: PAPI_LD_INS / PAPI_TOT_INS
* Ratio of FP OPS: PAPI_DP_OPS / PAPI_TOT_INS
* FP intensity: PAPI_DP_OPS / PAPI_LD_INS
