Survey Database - BETA
======================

*survey_database* is a tool used to export existing *survey* runs into an SQLite database for tracking
of test results over time. *survey_database* stores both measurement results as well as metadata results.
To plot the results of the database, use the *survey_plot* tool.

Here are the options available for *survey_database*:

* --add-run <prefix>: Add the specified run to the database
* --add-all-in-dir <directory>: Add all runs in the specified run to the database
* --list-runs: List all the runs in the database
* --remove-run <run number>: Remove the specified run number from the database. This run number is found with the list-runs command.
* --database: Specify the database to perform the action on. If no database is specified, the default database from the default project will be used.
* --config <file location>: Specify the extractor config file to filter the data. Be default, all data is added to the database. If not data is needed, you can filter the data by altering the extractor.yaml config file found in your project space.
* --output-to-file: Output the results of the *survey_database* command to a file instead of standard output.
* --tags: Add a comma-delimited list of tags to the runs when adding them to the database. (Used with the --add-run and --add-all-in-dir commands.)
