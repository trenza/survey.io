Survey Report
=============

*survey_report* is a script that is very similar to *survey_viewer*. Use it to generate a *survey* report
from previously-measured *survey* results. However, *survey_report* works on the json report files
(metadata and report) generated by *survey*. *survey_viewer* requires the original raw csv files generated
by the *survey* collector.

*survey_report* has the following parameters:

* prefix: This is the existing *survey* report prefix. A metadata file with the format <prefix>-metadata.json and a report file with the format <prefix>-report.json are required.
* --output-to-file </path/to/file>: When toggled, this will output the text and block report to the specified file instead of standard out.
* --block: When enabled, the block report will be included. The block report contains the min/max/avg of all *survey* measurements.
* --disable-text: When enabled, this will omit the text report. This is a concise summary of the *survey* results.
* --disable-html: When enabled, this will omit the html report. This is a html-based version of the text report.
