Survey Project - BETA
=====================

*survey_project* automatically organizes data into a survey project space that consists of:

* Configuration files
* Database directory used for postdoc data extraction
* Log file containing a list of runs and run locations

The default project space is in the user's $HOME/.survey directory. The file project.yaml in the base project
directory defines the structure of the project. By default, this structure is:

.. code-block:: console

   project_directory
     project.yaml
     log.yaml
     |--survey_config
     |  |-config.yaml
     |--extractor_config
     |  |-extractor.yaml
     |--databases
     |  |-default.db

The directory names can be changed by editing the project.yaml file.

The survey-project application will allow you to list project spaces, view a project,
add a new project, and remove a project. Within a project, you can use survey-project to
list all databases in a project, create a database, set the default database, remove a
database, and find an existing database.

*survey_project* has the following parameters:

* --list-project: This will list all the projects that have been recorded into the user's project list. This is also the default behavior of the application.
* --create-project <directory>: This will create a new project and associated config, log, and database files. The base directory will be used as the project name.
* --add-project <directory>: This will add an already existing project to the user's project list. You may only add projects with unique project names (base directory names).
* --view-project <project_name>: This will display the files and directories associated with the project.
* --remove-project <project_name>: This will remove the project from the user's project list. It will not delete any files or directories.
* --create-database </path/to/file>: This will create a new database and add it to the specified project. If no project is specified, the default project will be updated.
* --list-databases: This will list the databases associated with the specified project. If no project is specifed, the default project will be displayed.
* --set-default-database <database_name>: This will update the default database for the specified project. If no project is specified, the default project will be updated.
* --remove-database <database_name>: This will remove the specified database from the specified project. If no project is specified, the default project will be updated.
* --find-databases: This will list all databases that exist in the project directory. A description of whether the database is included in the project file will also be noted.
* --project <project_name>: Specify the project for any of the above commands.
* --output-to-file <filename>: Write any output of this application to a file instead of standard output.

Project Log
-----------
When *survey* runs, it creates a log entry in the current project with basic information about
the run including the launch time, end time, executable, prefix, user name, cpu model, and any
included tags. If no project is specified, this info is added to the default project, which is
usually in the user's $HOME/.survey directory.

To specify logging to a different project, use --project with the *survey* command.

.. code-block:: console

   survey_runs:
    1:
      launch_time: 2023/08/01, 22:18:20
      end_time: 2023/08/01, 23:21:28
      executable: /home/this_user/LULESH/build/lulesh2.0
      prefix: /home/this_user/runs/lulesh-2.0-survey-1
      user_name: this_user
      cpu_model: '158'
      tags: null
