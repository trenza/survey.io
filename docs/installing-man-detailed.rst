Detailed Instructions for Installing *survey* with manual process
=================================================================

Installation process: *survey* dependencies
-------------------------------------------

The external packages will need to be either built and installed or
if they are installed on the build system, one can point the cmake
arguments to that install path.

To build *survey* collector, there are software dependencies required:

 - LIBMONITOR - is a library providing callback functions for the begin and end of processes, threads, fork, exec, etc.  It provides a layer on which to build process monitoring tools such as profilers. This specific build is needed as it requires a patch that we have included.

 - LIBIOMPDIR - This is the OMP interface that allows us to xxx if the compiler used to build the application does not have the capability. Normally not on systems, we need to install. We currently use llvm_omp9

Need to reference system install (or build if not there):

 - PAPI - This is normally installed on systems. We expect at least version xx and above. Check for install or build it if needed. Set cmake variable - see notes below

 .. code-block:: console

     papi_version
     PAPI Version: 5.7.1.0

 - MPI - This is usually already installed and in many cases, multiple types and versions. Set the cmake path for each build variable - ? MPIIMPL -? MPI module can also be loaded..

  - DOPENMPI_DIR=path

  - DMPICH_DIR=path - cray / intel /mpich

  - DMPT_DIR=path - sgi/hpe

  - DMVAPICH2_DIR=path

Python dependencies
-------------------

There are also dependent python packages required. These include pandas,
psutil, sqlalchemy, and upgraded pip. Many system installs already include
these packages. If they are missing, please install or request that they be
installed. If there are system access restrictions and you need to establish in
user space, we are also working to provide an externals package and install
process that includes these packages for the appropriate python version on your
system.

Installing *survey* dependencies
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Note:
cmake needs to be at 3.12 or greater

Download survey.ext.tar.gz from xxxxx

.. code-block:: console

  tar -xzvf survey.ext.tar.gz
  cd survey_ext

Set install directory:

.. code-block:: console

  export PREFIX=<install path>
  mkdir -p build
  cp build_survey_ext.sh build/.
  cd build

run the build dependency script:

.. code-block:: console

  ./build_survey_ext.sh

Installing *survey*
-------------------

Once you have library and python dependencies worked out, we can now build the
*survey* tool. The steps are described below (also in survey/install/README.manual)

within the provided *survey* package:

 - adjust the build_survey_config script for appropriate install paths for dependencies and other software paths such as PAPI and MPI.

To configure:

.. code-block:: console

  mkdir build
  cd build
  sh -x ../build_survey_config

If this is successful, go on. If not, re-adjust your config script, remove the
build dir and start over.

.. code-block:: console

  make
  make install

To run:
If you use modules, there is a cmake module that is created in the build
directory. Copy to where you reference your modules. if not, you need to adjust
your environment to access the run components.















Installation process: For a Cray platform - ?
---------------------------------------------
