Survey Options
==============

Survey has a number of options that can be used to customize the run.

Collection Options
------------------

* --counters: Specify a custom list of counters. Accepts both papi and papi_native counter names
* --disable-papi-multiplex: Disables the papi multiplex counters
* --disable-papi
* --disable-io
* --disable-mpi
* --disable-ompt
* --disable-kokkos
* --enable-acclerator: Enables NVIDIA-SMI collector. Only works if an NVIDIA lib is linked to the executable and nvidia-smi is in the path.

Mem Options
-----------

* --enable-mem: Enables tracking memory calls

Top-Down Options
----------------

* --top-down: First level top-down analysis
* --top-down-fe: Front End top-down analysis
* --top-down-bs: Bad speculation top-down analysis
* --top-down-ret: Retiring top-down analysis
* --top-down-mem-bound: Backend memory top-down analysis
* --top-down-core-bound: Backend core bound top-down analysis
* --top-down-all: Complete top-down analysis. Requires many counters. Broadwell only.

Viewing Data Options
--------------------

* --info: Get info on measurement item
* --csv-only: Only output csv results
* --json-only: Only output json results
* --analyst-report: Output report followed by advice using *survey* results
* --block-report: Include block report (values of individual metrics) in output

Debug Options
-------------

* --debug_collector: Includes debugging output from the collector runtime
* --debug_monitor: Includes debugging output from the libmonitor runtime

Misc Options
------------

* --no-collector: Collect metadata only. No collector or measurement reports generated.]
* --output-to-file: Output to file instead of standard output
* --prefix: Specify custom prefix
* --config: Specify custom config file
* --tag: Run will be tagged with specified tag
* --timestamp: Prefix will include timestamp instead of incrementing index
