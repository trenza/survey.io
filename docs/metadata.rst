Metadata
========

This data is collected before the *Survey* run

Executable
----------

* command line: The command line used with survey (without survey arguments)
* executable path: The path of the executable as determined by *Survey*
* executable_ldd: The libraries linked to the executable (determined by ldd)
* mpi: Boolean declaring whether the executable is run as an MPI application
* libmpi: The MPI library linked to the executable (if any)
* openmp: Boolean declaring whether the executable is run as an OpenMP application
* accelerator: Set to NVIDIA if accelerator found. Only NVIDIA is supported at this time
* launch_time: Datetime to nearest second when run was launched
* end_time: Datetime to nearest second when run was completed
* survey_arguments: Arguments used with survey for run
* config_file: Survey config file used with run
* num_threads: Number of threads of execution
* num_ranks: Number of ranks
* num_processes: Number of processes used in the run
* rank_list: list of all the rank numbers
* thread_list: list of all the thread numbers
* process_list: List of all the process IDs
* host_matrix: Map of ranks attached to hosts/nodes and threads attached to ranks
* host_rank_mapping: Map of which ranks are attached to which host
* program type: Program type as determined by *Survey*. This affects how the data in the report is displayed.

Hardware
--------

* cpu_info: Info from linux command: lscpu. Hyperthreading is determined by counting physical cores and logical cores. If the numbers are not equal, hyperthreading is assumed. The module type is determined from a cpu table that maps model numbers (from lscpu) to model names
* mem: Info from linux command: lsmem --summary
* filesystems: Info from linux command: df -h .
* lspci: Info from linux command: lspci -nn -m (filtered for VGA, 3D, Infi*, Ether*)


System
------

* Survey version
* node_name: Name of the node used to start the *Survey* run. From Python command platform.node
* user_name: From Python command: getpass.getuser
* os_name: From Python command os.uname
* os_release: From Python command os.uname
* os_version: From Python command os.uname
* os_machine: From Python command os.uname
* limits: System limits from Python command resource.getrlimit
* env_variables: Environmental variables before run
* slurm_info: If env variable SLURM_JOB_ID is present, and the command scontrol is
  available, the following is run: scontrol show job SLURM_JOB_ID
* affinity: Map containing condensed list of ranks/processes, the threads attached
  to them, and the affinity core values for each thread. Affinity values are collected
  before and after the run for each thread.
