Parsing the Output of an Application
====================================

Some applications have important information or self-measurements
included in the output of the program that the user may want to
add to the output of survey. This can be done through a custom
written parser plugin.

Plugin Requirements
-------------------

The plugin has a number of requirements.

* It needs to be a Python 3 file that imports plugin_collection and
  includes a class that inherits plugin_collection.Plugin.
* It needs to be included in the survey/plugins directory.
* It needs to have an __init__ method that sets self.exe_name to be
  the name of the executable that will be called. This identifies
  the parser and connects it to the executable, so it is important
  for this name to be set exactly.
* It includes the function parse_std_out with an input of
  std_out_list. This input consists of a list containing each line
  of the application's output.
* The parse_std_out method returns a dictionary consisting of
  of a set of key values that will be included in the survey
  output json file. This dictionary can contain sub-sections
  if desired.

An example of such a plugin can be found in the survey/plugins
directory in the file ior.py.
