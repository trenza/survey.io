
Introduction
^^^^^^^^^^^^
Building survey at a site that has restricted internet access requires a set steps
that are outlined below.  The process consists of getting the
source for survey and all the dependencies into a mirror, moving
the mirror to the build platform, unpacking the mirror,
initializing spack, telling spack where to find the mirror, setting
up an externals file for components you don't want spack to build, and
finally issuing the spack install command.  spack will create a
module file that loads the runtime environment for survey.  All of this
is described in the sections below.

If you have internet access, then please skip steps 1 and 2.

1.-Creating a survey spack mirror source on a machine with internet access

2.-Setting up the spack mirror to be used by spack on the build platform

3.-Setting up the externals file on the build platform

4.-Intializing/Setting up spack for build including finding compilers to use

5.-Issuing the spack install command with the proper arguments

6.-Finding and using the spack created survey module file


Creating a survey spack mirror on a machine with internet access
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

General information on creating a spack mirror can be found in the pages
listed below.  An important tip is when creating the mirror on the
machine with internet access is to make sure that the externals file
on that mirror creation platform doesn't have entries for items you
want to bring to the build (one with no internet access) platform.
If spack sees entries in the externals file (~/.spack/linux/package.yaml
in most cases) spack will not add those components to the mirror sources.

.. toctree::
   :maxdepth: 2

   sandia-mirror-create.rst

A typical spack creation command is as follows:

.. code-block:: console

   date=09-20-2022
   spack mirror create -d /home/<user>/survey-mirror-$date -D survey +openmpi+mpich
   tar -cvf /home/<user>/survey-mirror-$date.tar /home/<user>/survey-mirror-$date
   gzip /home/<user>/survey-mirror-$date.tar
   scp /home/<user>/survey-mirror-$date.tar.gz <somewhere>


Setting up the spack mirror to be used by spack on the build platform
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2

   sandia-mirror-build.rst


Setting up the externals file on the build platform
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For survey builds these externals can be specified in a package.yaml
file that will prevent spack from building these existing installations of
openmpi, mpich, libtirpc, and krb5.  The externals file contains paths or
module commands representing how to find the installations of each component
or library specified.  Arguments are used to indicate what architecture
and compiler the specification applies to.

.. code-block:: console

 packages:
   all:
      providers:
         pkgconfig: [pkg-config]
   openmpi:
      externals:
      - spec: "openmpi@1.10%gcc@10.2.1 arch=linux-rhel7-broadwell"
        modules:
        - openmpi-gnu/1.10
      - spec: "openmpi@3.0%gcc@10.2.1 arch=linux-rhel7-broadwell"
        modules:
        - openmpi-gnu/3.0
      - spec: "openmpi@3.1%gcc@10.2.1 arch=linux-rhel7-broadwell"
        modules:
        - openmpi-gnu/3.1
      - spec: "openmpi@4.0%gcc@10.2.1 arch=linux-rhel7-broadwell"
        modules:
        - openmpi-gnu/4.0
   mpich:
      externals:
      - spec: "intel-mpi@2018%gcc@10.2.1 arch=linux-rhel7-broadwell"
        modules:
        - intel-mpi/2018
      - spec: "mpich@2018%gcc@10.2.1 arch=linux-rhel7-broadwell"
        modules:
        - intel-mpi/2018
   libtirpc:
      externals:
      - spec: "libtirpc@1.2.6%gcc@10.2.1 arch=linux-rhel7-broadwell"
        prefix: /usr
   krb5:
      externals:
      - spec: "krb5@1.0.1-9%gcc@10.2.1 arch=linux-rhel7-broadwell"
        prefix: /usr



Intializing/Setting up spack for build including finding compilers to use
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To put spack in your path you can do a sequence similar to this:

.. code-block:: console

   export PATH=<spack_root>/bin:$PATH


To find the compilers and add them to the compilers.yaml file in ~/.spack/linux
run the spack compiler find command.


.. code-block:: console

   spack compiler find

You must load the tce module (NNSA labs) before trying to build with spack (for python).


.. code-block:: console

   module load tce

Issuing the spack install command with the proper arguments
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Typical install lines for building survey on build platform are of this form:

.. code-block:: console

   spack install --no-cache survey@1.0.5-openmpi-4.0 %gcc@10.2.1 +mpi ^openmpi@4.0
   spack install --no-cache survey@1.0.5-openmpi-3.1 %gcc@10.2.1 +mpi ^openmpi@3.1
   spack install --no-cache survey@1.0.5-openmpi-3.0 %gcc@10.2.1 +mpi ^openmpi@3.0
   spack install --no-cache survey@1.0.5-openmpi-1.10 %gcc@10.2.1 +mpi ^openmpi@1.10

The version specification above, such as *survey@1.0.5-openmpi-1.10* must match a tarball verion
in the survey mirror sources.   That tarball can be created when the mirror was created or it
can be added after the mirror is brought to the build machine.  In the latter case one can copy
the survey@1.0.5.tar.gz file to a new survey@1.0.5-openmpi-1.10.tar.gz file.

.. code-block:: console

   cp survey@1.0.5.tar.gz survey@1.0.5-openmpi-1.10.tar.gz
   cp survey@1.0.5.tar.gz survey@1.0.5-openmpi-3.0.tar.gz
   etc..

This allows spack to find the tarball with a more detailed name.   Also, the survey package.py
file needs to have a version entry that matches the survey@1.0.5-openmpi-1.10.tar.gz file.
That would look like this:

.. code-block:: console

    <from survey/package.py>
    version('1.0.5-openmpi-4.0', branch='1.0.5')
    version('1.0.5-openmpi-3.0', branch='1.0.5')
    version('1.0.5-openmpi-3.1', branch='1.0.5')
    version('1.0.5-openmpi-1.10', branch='1.0.5')



Finding and using the spack created survey module file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The spack module file that is created by spack based on
the information and settings in the survey/package.py
file can be found in the spack tree at this location
for the eclipse platform:


.. code-block:: console

   <spack_root>/share/spack/modules/linux-rhel7-broadwell

The next steps are to load the survey module file using these steps:

.. code-block:: console

   module use <spack_root>/share/spack/modules/linux-rhel7-broadwell
   module load  <spack module filename>
