*survey*  Components and Collectors
===================================

**Components**
--------------

There are several different components to *survey*:

* Collectors
* Summarization and Analysis
* Querying and Reporting


.. image:: files/components.png
  :scale: 110%
  :align: center

**Collectors**
--------------

* Metadata collector (meta-c) from the computing and runtime environment,
* Core runtime collector library (core-c) that runs during the execution of the application at the thread level,
* Integrated collectors (int-c) for specific API intefaces such as kokkos
* Specific external collector (ext-c) integrations such as nvidia-smi
* Ability to create a paser plugins (app-c) to collect application standard output.

-Metadata (meta-c)
------------------

The performance of an application can be affected significantly by its
environment. The specifications of the CPU and memory capabilities on the
compute nodes are among the more obvious contributors to the application’s
performance, but the performance can also be affected by such factors as the
file system and the libraries linked to the application. Furthermore, the
environment used by the application can change over time, sometimes without the
knowledge of the user.

For this reason, *survey* collects information about the computing environment
before the application runs. This metadata is stored in JSON format in a
metadata file in the output folder that also contains the results from *survey’s*
collector. This allows it to be used by *survey’s* data viewer and included in the
metadata section of the output JSON file.

When collecting the metadata, *survey* relies on commonly used UNIX utilities. In
some cases, one or more of these utilities may not be available on the machine
so data will not be collected.

* ldd\: Libraries linked to the executable
* scontrol\: Information from the Slurm workload manager
* lsmem\: Ranges of available memory
* df\: File system disk usage
* lscpu\: CPU architecture information
* lspci\: Detailed information about all PCI buses and devices in the system

Some additional metadata information is collected using Python tools:

* The command line entered by the user
* The name of the executable, as determined by *survey*
* Where or not MPI and/or MP are used by the application
* The launch time of the application
* The name of the controlling node
* The username who launched the application
* The environmental variables at the time the application is launched
* The operating system of the controlling node including name, type, and version
* Whether or not hyperthreading is enabled
* The model name of the processor of the head node if it is Intel, e.g., Broadwell, Haswell, or Ivybridge.

-Core Collectors (core-c)
-------------------------

There are multiple sources of data for *survey* collectors. The initial source is
from within the application.

During the application run, *survey* uses its runtime collector to gather:

* rusage data
* PAPI hardware counter data
* POSIX I/O data
* memory data (libc)
* MPI data
* OpenMP data through the OMPT API
* Kokkos data through the kokkos tools API
* DMem data for the lifetime of each thread of execution.

When the execution of the application is completed, a separate
CSV file for each thread is created, detailing the results, and placed in the
sub-directory described above. The directory structure is shown in Figure 1.

You can disable/enable particular types of data collection using one or more of the
following on the *survey* command line:

- \--disable-papi
- \--disable-io
- \--disable-mpi
- \--disable-ompt
- \--disable-kokkos
- \--enable-mem


-Integrated (int-c) and External collectors (ext-c)
---------------------------------------------------

Similar to the system and environmental metadata described, we have found that
to provide the data needed for a holistic analysis, we need to tap into other
tool interfaces or utilize thier collection mechanisms to add to what is collected
(These may be tools from architecture/component vendors or third party providers).
These tools are usually very targeted on where they can
be run and even if they are present in the application or on the system being
worked on. Our intent
is to create a collector that will determine if the the  apiis available within
the application or an external
collection tool is available, provide survey runtime options (if needed) and
collect the data as an additional data store and also to extract to add to the
summary CSV and JSON files.

--- kokkos collector
--------------------

The kokkos collector is an integrated collector that taps into the kokkos
performance API and summarizes specific metrics.  survey tracks kokkos parallel_for,
parallel_scan, and parallel_reduce kernels and reports the total time and total
counts for each type of kernel.  Derived metrics are created for the total time spent
in kokkos kernels, the total number of times a kokkos kernel was executed and the
percentage of the total time that was spent in kokkos kernels.

--- nvidia_smi collector
------------------------

The CUDA external collector uses the NVIDIA-smi tool to gathers usage information
for each GPU being used by the application.  survey creates a GPU based CSV file
that is processed by survey when the application finishes.   The analysis output
of the GPU data is stored in the survey application result JSON file.


-Application collectors (app-c)
-------------------------------

The third collection source is from the application run time. There is usually
much appropriate data that is output from an application as it is being run.
This is either to stdout or to output files and may include meaningful numeric
data pertaining to the progress or result, the phase the application is on, or
other. This is important data that when integrated with the run and system
performance metrics, provide a more integrated view of the run process and
application behavior. We will provide the mechanism to develop these collectors
and integrate into the survey runtime. Application developers/users are the
target for these collectors and are best situated to develop. These collectors
(plugins) will not be managed through the survey repo and should be managed
by the application support staff. VASP is the initial app-c that we are working
with.

**Summarization and Analysis**
------------------------------

This is done at the end of an application run and begins with the processing and
summarization of the data across nodes and
what is collected at the thread level, plus what needs to be done for data from
integrated and external collectors. From this, the metadata.json file is created
as is the csv summary files which feed the report.json file.

-Runtime
--------

It is important to
note that data collected is very much based on config options that are entered on
the command line or can be pre-configured in a config.yaml file that can be used
at run time (see configuration section).

-Post-runtime
-------------

Further analysis can be done. Reports and plot dashboards can be generated from
the survey json files created at runtime. Deeper analysis can be done by by using
the survey_viewer. This option uses the existing thread level csv files and
allows for re-summarization and analysis - it will create a new set of json files
for the run. If desired, the analysis options used by Survey viewer can be saved
into an extractor.yaml configuration file (see configuration section).

**Query and Reporting**
-----------------------

There is a large amount of metadata and metrics that are collected. This can be
overwhelming as to
what you should look at and track to provide the information you need to ensure
that your application is doing well.  Consider what you are attempting to discover
regarding your application under different architectures, run times, compilers
build and run options, etc.. Then review the metrics that are of interest and assess
the ouptut. As part of your workflow, you should select the collection options to be
used when you run survey (you can also pre-configure your choices into config.yaml
the you cna name and call at run time using the --config option). You can also
limit the metrics that are collected into the json files by using an extractor, either
at run time or post-analysis.

The survey json summary files can be recreated from the CSV thread files by using
the survey_script. This will re-create the summary json files and you
can use an extractor file that will allow you to limit the metrics posted as well
as adding options such as outlier calculations.

Reporting from the json files can be done by running survey_plot or querying your
runs through survey log list, then a survey view run#. This area is being worked
on and we will provide more capability shortly.
