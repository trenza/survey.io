Frequently Asked Questions
==========================

Error code processing
---------------------

**Question:** Are test failures simply caused by non-zero return codes from
any of the commands in the script definition, similar to how you would expect
with Jenkins?

**Answer:** When testing for failures or if  a non-zero status is expected
from a command you will either need to script around this with Bash or use
some sort of additional tool in your script. This is the same as Travis
and from our understanding similar to Jenkins.

Specify where GitLab CI pipeline runs
-------------------------------------

**Question:** How do I specify where the GitLab CI pipeline will run?

**Answer:** There are two very important GitLab clause's that identify where
the pipeline will be executed.  The two clauses are named **tags** and
**variables**.  In the GitLab CI file snippet example below the usage of
tags and variables (particularly SCHEDULER_PARAMETERS) is shown.

  .. code-block:: yaml

    variables:
        SCHEDULER_PARAMETERS:  "--nodes=1 -p ecp-p9 -t 0:10:00"

    stages:
      - quickstart-build
      - quickstart-test

    generic-mpi-build:
      stage: quickstart-build
      tags: [generic, slurm, ecp-ci]

.. note::
  SCHEDULER_PARAMETERS is a batch runner specific variable that
  is used for defining arguments that will be used when submitting
  a job to the appropriate scheduling system. For example, the below
  variable with Slurm will be:
  `sbatch --nodes=1 --partition=x86_64 <other options>`

  Additionally, admins have the ability to define expectations for
  the variable however they want. Runner administrators can set a
  new variable name that serves the same purpose, if the appropriate
  variable is not found you will be warned during the job's execution.

.. note::
  Tags are used to select specific Runners from a list of available
  runners. The admin who deploys / registers the runners defines these
  tags and by specifying them we can ensure our jobs are deployed on
  the correct system. You can identify the details of the runners
  available to the project by: Settings > CI/CD > Runners tags:

Targeting multiple platforms
----------------------------

**Question:** I'm curious what best practice is for writing tests that can
target different compute platforms and that require different system-specific
command line parameters for correctly running the tests.  We have a compelling
reason to want to do this in our project tests, but it's not even clear to me
how I would do something as simple as specifying different project allocations
on the job submission command line on different systems. I know tags can
be used to control which jobs run on which systems, but I don't see how
we can construct any sort of conditional logic based on which tags a given
platform meets.

**Answer:** There is an example of how this can be done here:
`targeting multiple resources <../examples/target-multi-resources.html>`_.

That description shows that it is a combination of tags and scheduler
variables that describe each platform.  That section of code is given
a label and is then included where one needs it using the GitLab extends
clause. Please see the detailed example by following the above link.
