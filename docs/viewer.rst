Survey Viewer
=============

*survey_viewer* is a script that can be used to generate a *survey* report from previously-measured
*survey* results.  *survey_viewer* requires the original raw csv files generated by the
*survey* collector.

*survey_viewer* has the following parameters:

* view_dir              directory of existing data to analyze.
* -h, --help            show this help message and exit
* --block-report        Include block results in report
* --analyst-report      Include analyst results in report
* --io                  view io details
* --memory              memory details
* --papi                papi measurements and calculations
* --summary             summary details including timing, etc
* --rusage              rusage details
* --mpi                 mpi details
* --ompt                omp details
* --kokkos              kokkos details
* --top-down            top-down analysis
* --ranks RANKS         ranks to analyze (e.g. "x,y,z" or "x:z" or "x" or "all")
* --threads THREADS     ranks to analyze (e.g. "x,y,z" or "x:z" or "x" or "all")
* --processes PROCESSES processes to analyze (e.g. "x,y,z" or "x:z" or "x" or "all")
* --enable-accelerator  Parse gpu csv data
* --config CONFIG       location of config file
* --project PROJECT     Specify the *survey* project file
* --csv-only            only output results in csv format
* --json-only           only output results in json format

