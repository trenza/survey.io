Survey Plot
===========

*survey_plot* allows you to plot multiple *survey* results with bar graphs and histograms showing trends over time.
Results are shown as HTML pages, with one page created for every application in the SQLite database. This command
is used with *survey_database* to add the runs into the database for use with *survey_plot*.

Unless otherwise filtered with the --config or --tag options (as described below), a bar plot and histogram will be
generated for each measurement found in the database. These measurements are sorted by the launch_time from the
run's metadata.

The *survey_plot* options are:

* --project: Specify the name of the project containing the database. If none is specified, the default project will be used.
* --database: Specify the database that contains the data for the plots. This database needs to be included in the project. If no database is specified, the default database for the project will be used.
* --config: Specify the extractor config file. This will filter the data used when creating the plots.
* --output-to-file: Specify the directory to output the results of the plot. If none is specified, one will automatically be generated in the current working directory.
* --tag: Plot only the results with the specified tag.
