Site Support
============

Installation process
--------------------

spack
^^^^^

We are initially providing an installation process using the
`spack build manager <https://github.com/spack/spack/wiki>`_, which
provides the ability to ensure that software dependencies are
appropriate for the system and application environment that you
are utilizing. As an individual installer, the spack install will
use your spack tree to install the appropriate software and create
modules files. If you are installing for a larger system
infrastructure and already have a spack deployment environment,
the survey spack package creation file (package.py) can be integrated in.
The following sections provide details on the steps to install
survey using spack across different build platform environments.
Note that a spack mirror is used to hold the source files needed to
build survey on platforms where general internet access is not allowed.
A spack mirror is created on a platform where access is allowed and then
that source is moved onto the platform where access is not allowed.

We will provide access to the package.py file and the tar file containing
the survey tool.

.. toctree::
   :maxdepth: 2

   installing-detailed.rst


manual
^^^^^^
If one choses not to use the spack build system to build survey, there
is a manual method to build the survey performance tool using cmake.

The survey build depends on Python3, cmake 3.12 and above, libmonitor (with
patches), papi, and the OMPT interface.  The spack build
described in the sections above automatically builds those
components and uses them in the survey build.  With the
manual build process we have an external package that includes libmonitor
and OMPT. We expect that you have a local MPI and PAPI installed. If
not those will also need to be built.


.. toctree::
   :maxdepth: 2

   installing-man-detailed.rst


Subscription file (TBD)
-----------------------

Depending on the scale of the installation, we are making it simple to deploy
subscription tokens on your platforms to allow quick validation of your
license. Based on a machine/platform fingerprint, we will register and provide
a run token that can be installed through the spack process.

Machine and Software environment nuances
----------------------------------------

As mentioned earlier, HPC systems are known for unique hardware architecture
combinations and interesting software stacks which are always a challenge. In most
cases there should be no installation issues, but if there is we will work
with you through the install.

MPI considerations
^^^^^^^^^^^^^^^^^^

A MPI implementation's data structures are used by survey to gather and record MPI timings.
Each MPI implementation has their own specific internal data structure format, therefore
survey needs to know the specific MPI implementation that the application being
monitored was built with.   No user intervention is needed if only one MPI implementation
was used in the survey build, which is the default setting when building with spack.
However, if building manually and if multiple MPI implementations were specified
during the build, then the **SURVEY_MPI_IMPLEMENTATION** environment variable
needs to be set in order to indicate to survey what MPI implementation is being
monitored.  The **SURVEY_MPI_IMPLEMENTATION** environment variable is needed for
survey to properly process the gathered MPI performance data for output displays.
Typically this environment variable is set in the survey module file so that
it is explicitly known to the user.  Typical values for the **SURVEY_MPI_IMPLEMENTATION**
environment variable are one of the following: openmpi, mpich, mpt, mvapich, or mvapich2.
Cray MPI is specified as mpich and IBM spectrum MPI is specified as openmpi.

Another consideration is abi compatibility with the MPI being used to run the
application. It is recommended that a seperate survey be built for each major
version of an MPI implementation. In the case of OpenMPI with various modules
available for version 3 and 4, it is best to build survey targeting one of the
installs for each of the series (a gcc build is a good default if available). If
a module is created, including the MPI implementation and major version in the
name would make it clear to the user which to use.

Another consideration is to build *survey* with the MPI installed by your systems
staff. They may have built MPI with integrations for the system schedular that would
allow better resource integration.

Program counter considerations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

survey has a default list of
`PAPI  <https://icl.utk.edu/papi/>`_ named hardware counters that it uses to gather
perfomance counter information.  This list is different on various hardware platforms
due to the different underlying hardware support.   survey also allows a list of
hardware counters to override that default list.   The user specified list can include
PAPI hardware counter names and platform specific native hardware counter names.

Another consideration is that there are hardware counters that share internal hardware
resources which will cause conflicts when the hardware counters sharing those resources
are specified on the survey command line.   When that happens one or more of the counter
values may not show up in the output because the hardware couldn't collect the counter
values due to the conflict.
