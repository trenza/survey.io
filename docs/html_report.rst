Survey HTML Report
==================

The Survey HTML Report is a summary of the run, containing
the most important results of the run. It is not a comprehensive report; the
metadata and report json files will contain information that is not
shown in this HTML summary report.

The HTML Report contains:

* Summary information
* Pie charts showing the balance of the MPI, OpenMP, I/O, CPU/Other elements
* Total time breakdown of MPI, OpenMP, I/O, and Other
* OpenMP breakdown
* I/O Breakdown
* RAM memory usage
* CPU Counter Analysis
* Affinity
* FOM: If the output of the application was parsed with a plugin and if the results contained a line
  with "FOM" (capitalization does not matter), then those results will be included here.

When the application has OpenMP elements, the results are divided up into the results of the master thread
and the results of the worker threads, represented as different labeled columns for each section.

Summary
-------

The Summary section contains the following data, taken from the metadata section of the test results.

* Host Machine
* Run Start Time
* Overall Job Time
* SLURM Job Name (if applicable)
* SLURM Job ID	(if applicable)
* Resources: This is a combination of several fields in the metadata including the CPU type of the host
  machine and the number of nodes in the run. The host machine is evaluated for the number of sockets,
  the number of cores per socket, and the number of threads per cores.
* The command used to generate the run
* The amount of memory on the host machine

OpenMP Time Breakdown
---------------------

This section breaks down the OpenMP results into the Compute time, Serial Time, and the Barrier time.
The Compute time is calculated by subtracting the Serial Time and the Barrier Time from the Total OpneMP time.

RAM Memory Usage
----------------

The section contains the min/max/avg of the memory used by the application.

CPU Counter Analysis
--------------------

This contains several important results from the CPU counter calculations.

* Instructions Per Cycle: PAPI_TOT_CYC / PAPI_TOT_INS
* Computational Intensity: PAPI_TOT_CYC / PAPI_TOT_INS
* Ratio of FP Ops: PAPI_DP_OPS / PAPI_TOT_INS

Total Time Breakdown
--------------------
This section contains a breakdown of how much time the application spent in
MPI, OpenMP, and I/O activities. The remaining CPU intensive time is labeled as "Other".

I/O Breakdown
-------------
The I/O breakdown shows the time time and the amount of data used in reads and writes.

Affinity
--------
The Affinity section shows the affinity results collected at the beginning and the end of the run.
The node, ranks for each node, threads for each rank, and the cores for each thread are shown.
