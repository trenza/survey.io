Survey Extractor - BETA
=======================

The *survey_extractor* has the capability to further refine the view of the Metrics and Metadata
collected. Currently it is in beta-testing mode and changing rapidly. We are seeking feedback to
improve its functionality of selecting, re-formatting, comparing runs, and performing analytics
through feeds to a SQL-lite database with graphing options.

We are continuing to add collectors to *survey* for additional data that represents the environment and the
application's use of compute resources. Different users have different needs, and the extractor
is positioned to help users extract the data that they require for their particular analysis needs
and feed into tracking databases or other tools. The extractor.yaml file described below will allow
you to chose the exact Survey data that you want to extract. Other functionality such as comparing
multiple runs of data and feeding the data into the *survey* sqlite database are positioned to provide
further analytic capabilities in the future.

Note - some functionality currently requires file repackaging (copying data to directories and re-naming)
to more easily use current options. This is a work in progress.

As the survey_extractor interface is changing, make sure to look at help options:

.. code-block:: console

    $ survey_extractor -h

Below are the current notes to help you get started.

Extractor yaml setup
--------------------

Survey extractor utilizes the extractor.yaml config file to identify which metrics and metadata that
you are interested in. If desired, you can set up different extractor.yaml files for different extraction
and analysis tasks.

.. figure:: files/extractor.yaml
  :scale: 100%
  :align: left
  :alt: extractor.yaml

  For now - download and modify this one.

*survey_extractor* will use a config file of a *survey* project. The default *survey* project will
usually be located in the user's $HOME/.survey/ directory. You can specify a different project using the
--project parameter. This will use the default config file for the specified (or default) project. To
specify a particular configuration file, use the --config parameter.

Your edited yaml config file should specify the *survey* data that you want to use with *survey_extractor* to create
a filtered, consolidated report of measurement data. If desired, you can create multiple versions of the extractor
config file for different purposes.

Survey Extractor functions
--------------------------

* <target>: Specify the *survey* prefix that you want to extract the results from
* --project: Specify the project file. If no project is specified, the default project will be used.
* --config: Specify the extractor config file. If no config file is specified, the default config file for the specified (or default) project will be used.
* --output <dir>: Specify the location of the output results
