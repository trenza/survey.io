Running *survey*
================

To run *survey*, just prepend *survey* and any desired *survey* options to
the command line that you normally use to run your application. For instance:

.. code-block:: console

    $ survey mpirun -np 32 ./mpiapp

The above command will collects all the default *survey* metrics for the
application "./mpiapp." Hidden from the user, the *survey* tool operates by
utilizing LD_PRELOAD to wrap function calls in the application and link in the
*survey* libraries needed to analyse the your application.

Run Options
-----------
*Survey* includes options to disable and enable specific performance data
collectors and to change how the data is displayed and output.

Note: Whenever *survey* measurements result in a 0 value in all threads, that
measurement will not be included in the results.

.. code-block:: console

    $ survey --help

usage: *survey*
    survey [-h] [--counters COUNTERS] [--disable-papi-multiplex] [--disable-papi]
    [--disable-io] [--disable-kokkos] [--disable-mpi] [--use-mpi-ext]
    [--disable-ompt] [--use-survey-omp-rt] [--enable-mem]
    [--top-down] [--top-down-fe] [--top-down-bs] [--top-down-be]
    [--top-down-ret] [--top-down-mem-bound] [--top-down-core-bound]
    [--top-down-all] [--info INFO] [--csv-only] [--json-only]
    [--debug_collector] [--debug_monitor] [--no-collector] [--no-analysis]
    [--analyst-report] [--no-log] [--block-report] [--output-to-file]
    [--prefix PREFIX] [--version] [--project PROJECT]
    [--config CONFIG] [--tag TAG] [--timestamp] [--name NAME]

positional arguments:
  target    Where target is the program you want to collect data
            from (e.g. mpirun -np 2 ./mpiapp)

collection options:
  Options to control the collector runtime. By default, *survey* collects data
  from all collectors except mem. The mem collector defaults to off because it
  can severely affect application performance when the application has extreme
  extreme allocation and deallocation behavior (which itself is an issue).

  --counters COUNTERS   A comma-delimited list of PAPI CPU counters to collect. Both papi and papi_native counters are allowed in this list. By default, *survey* collects data from the following CPU counters:

    * PAPI_TOT_CYC
    * PAPI_TOT_INS
    * PAPI_LD_INS
    * PAPI_VEC_DP
    * PAPI_DP_OPS
    * PAPI_FDV_OPS
    * PAPI_FP_INS
    * PAPI_FP_OPS
    * PAPI_L3_TCM
    * PAPI_L2_TCM
    * PAPI_L1_TCM
    * PAPI_TLB_IM
    * PAPI_REF_CYC
    * PAPI_REF_NS,
    * PAPI_FUL_CCY
    * PAPI_RES_STL
    * PAPI_TLB_DM
    * PAPI_TLB_TL
  --disable-papi-multiplex
                        Disables papi multiplex. When papi-multiplex is enabled, more counters can be measured in a run. However, the accuracy of the counter measurements is decreased. Not all systems support papi-multiplex.
  --disable-papi        Disables papi counter collection
  --disable-io          Disables tracking posix IO calls
  --disable-mpi         Disables tracking mpi calls
  --disable-ompt        Disables openmp ompt callbacks
  --disable-kokkos      Disables tracing kokkos callbacks
  --use-survey-omp-rt   Use the *survey* provided llvm openmp runtime with OMPT
                        api enabled
  --enable-mem          Enables tracking memory calls

topdown options:
  Options for topdown collection. This is only available for some intel processors.

  --top-down        	top-down analysis
  --top-down-fe     	front-end category of top-down analysis
  --top-down-bs         bad speculation category of top-down analysis
  --top-down-be         back-end category of top-down analysis
  --top-down-ret    	retiring category of top-down analysis
  --top-down-mem-bound  back-end sub-category of top-down analysis
  --top-down-core-bound
                        back-end sub-category of top-down analysis

viewing data options:
  Options for viewing data.

  --info INFO           Get info about a measurement or calculation,
                        where INFO is a derived metric name or PAPI
                        counter name.

output file options:
  Options for output files. Default is both csv and json.

  --csv-only        	only output results in csv format
  --json-only       	only output results in json format

debug options:
  Options to enable debug from the collector runtimes.

  --debug_collector 	Enable debug from the collector runtime
  --debug_monitor   	Enable debug from the libmonitor runtime

misc options:
  --no-collector      Run survey without the collector to grab metadata results only
  --no-analysis       Collect raw csv measurement results without additional analysis
  --analyst-report    Create analyst report which provides analysis using the survey results
  --block-report      Include block report (min, max, avg of all results) with output report
  --output-to-file    Output survey to file instead of stdout
  --prefix PREFIX     Specify the prefix used to create output files
  --project PROJECT   Save results to the log of the specified project
  --version           Find the survey version number
  --config CONFIG     Use specified survey YAML config file
  --tag TAG           User supplied value to be included in the metadata
  --timestamp         Add timestamp to survey output prefix


Run configuration and logging
-----------------------------

*survey*
also has options to create configuration options for user or project areas. this
provides the capability to create custom extractor.yaml and run_config.yaml files
and also allows to run logs to be updated in the appropriate work space (see section).
You survey run will also be logged to a log.yaml file to more easily track your
results. A log listing capability is described in the Post Hoc Analysis section.

Note that memory performance data collection is turned
off by default.  To turn on memory performance data collection add this
option (--enable-mem):

.. code-block:: console

    $ survey --enable-mem mpirun -np 32 ./mpiapp

Hardware Counter metrics
------------------------

In computers, hardware performance counters (HPC) or hardware counters
are a set of special-purpose registers built into modern microprocessors
to store the counts of hardware-related activities within computer systems.
Advanced users often rely on those counters to conduct low-level performance
analysis or tuning.  The description above is taken from and expanded upon `here: <https://en.wikipedia.org/wiki/Hardware_performance_counter#:~:text=In%20computers%2C%20hardware%20performance%20counters,level%20performance%20analysis%20or%20tuning.>`_.



.. toctree::
   :maxdepth: 2

   hw_counter.rst


