Data Collection and Analysis
============================

When *Survey* is run, the min, max, and average of the collector results are sent
to standard output (if the application was run sequentially, or if the results of the min,
max, and average are the same, only a single "value" is displayed.)

In addition to the standard output results, Survey also outputs the following where "prefix"
is the generated automatically by *Survey* from the executable name or is specified by the user.

If the application is not OpenMP based the report naming will be as follows:

* prefix-report.csv
* prefix-report.json
* prefix-metadata.json

If the application is OpenMP based the report will be split into multiple files where
the performance for the  OpenMP master thread and OpenMP worker threads are reported
in separate files but are combined in the report.json file.

* prefix-omp-master-thread.csv
* prefix-omp-worker-threads.csv
* prefix-report.json
* prefix-metadata.json

CSV Report
----------

The prefix*-report.csv files includes the min, max, and average results. Also included are:

* standard deviation
* variance
* the host/rank/thread for each measurement where the min and max results were recorded

JSON Report
-----------

The prefix*-report.json file includes the min, max, and average results. Also included are:

* standard deviation
* variance
* the host/rank/thread for each measurement where the min and max results were recorded
* NVIDIA SMI data (if included with run)

Metadata Report
---------------

The prefix-metadata.json file includes all the metadata collected before the run plus
the affinity data collected before and after the run. In addition, if you have an application
paser plugin created and in your plugin config area, the identified appliction output will
also be in this section. Additional detailed metadata information can be found below.

.. toctree::
   :maxdepth: 2

   metadata.rst
