Survey Log
==========

*survey_log* is an application that view and filter log files of a particular project as well as view
the measurement results of any run (as long as that run hasn't been moved or deleted).

If you run *survey_log* without any arguments, it will list all the runs in the log of the default project.

Here are the options for the *survey_log* command:

* --view <log number>: View the text report of the selected log number. The selected run needs to be located in the location specified in the log.

* --add <location>: This will add the specified prefix or all runs in a specified directory to the log. This command works recursively

* --project <project name>: Specify the project name. If this option is not set, the default project will be used.

* --tag: List all runs in the log with the specified tag.

* --exe: List all runs in the log with the specified executable name.

* --user: List all runs in the log run by the specified user.

* --before_launch: List all runs in the log that launched before the specified time. The format must match "Y/m/d, H:M:S"

* --after_launch: List all runs in the log that launched after the specified time. The format must match "Y/m/d, H:M:S"

* --output-to-file: Output the log to file instead of standard output.
