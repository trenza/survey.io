ECP CI Users
============

.. _HPC focused enhancements: introduction.html#enhancements
.. _administrator documentation: admin.html

Presenting documentation, best practices, and tutorials that are
focused on making the most out of ECP CI deployments. As such, information
presented here is centred on the `HPC focused enhancements`_ made to
to GitLab and deployed at several test locations. We will make ample reference
to GitLab's `official documentation <https://docs.gitlab.com/>`_ and strongly
encourage you to look there for details not covered in these pages.
For any questions on runner or  server deployment please see the
`administrator documentation`_.

Continuous Integration
----------------------

Understanding the `GitLab Runner <https://gitlab.com/gitlab-org/gitlab-runner>`_
is the key to making full use of the GitLab CI ecosystem. These sections
are targeted not just at those unfamiliar with using the GitLab runner but
also anyone who wishes to understand how the `HPC focused enhancements`_ will
affect their CI jobs.

.. toctree::
   :maxdepth: 2

   ci-users/ci-general.rst
   ci-users/ci-batch.rst
   ci-users/ci-faq.rst

Federation
----------

CI federation carries the goal of allowing software testing across
facilities in a secure manner. This process is targeted to allow ECP software
teams to mirror code from already established repositories to a centralized
trusted instance managed by `OSTI <https://www.osti.gov/>`_. We want to not
only allow projects to execute selected tests on their code base but
ensure it is done in an easily controlled manner using a combination of
GitLab supported tools and the
`federation enhancement <https://gitlab.com/gitlab-org/gitlab/-/issues/33665>`_.
The enhancements seek to enrich GitLab's existing identity model
to provide a connection between authentication provider, a user's
session, and a site runner responsible for executing CI.

.. image:: files/federation_1.png
  :scale: 35%
  :align: center

.. note::

    Detailed documentation and guides will be provided to help with all
    aspects of federation from creating an account to running CI. However,
    this will only be done once the official federated instance has been opened
    to wider availability. We will update this page with details as
    they become available.

Tutorials
---------

ECP CI tutorials aim to provide a look at how one might use traditional
GitLab functionally coupled with the ECP CI enhancements and available
HPC resources in a self-paced manner. If your unsure how to get started
using these tools this will provide an excellent place to start.

.. toctree::
    :maxdepth: 1

    tutorial/quickstart.rst

A complete ECP CI Startup Tutorial was given at the
`2020 ECP Annual <https://ecpannualmeeting.com/>`_ conference.
Future improvements for this tutorial structure will be aimed at
making all tutorials more self service and agnostic than our current offering
which is written to directly leverage conference specific test resources.

.. toctree::
    :maxdepth: 1

    tutorial/ecp_ci_startup.rst
