Hardware Counter Analysis in the *survey* Tool
==============================================

Hardware counter analysis in *survey* is based on papi counters which
are available on the architecture where applications are being run on.
Hardware counters keep track of counts or values from special registers
built into processors to allow tracking of hardware activity.  Users
through tools like *survey* and others can deduce how their application
is performing by tracking these values.  *survey* will cycle through a
list of specific hardware counters that make sense for the platform
that the application is running on, recording the hardware counter
values for the user application.  *survey* then reports these values
to the user and also computes other values derived from two or more
of the hardware counters it recorded.  These are called derived
metrics.

Most tools that perform this type of analysis do so on single-threaded
applications or single-threaded parts of an application.  HW-counter
analysis in the *survey* tool can be performed on MP and MPI
applications. The minimum, maximum, and average values of each
calculated measurements are included in the *survey* results.

PAPI Counters
^^^^^^^^^^^^^

By default, *survey* collects the results of a number of useful counters. However,
you can specify custom counters with the --counters option and a comma-delimited
list of desired counters, i.e. --counters “UNHALTED_CORE_CYCLES,
INSTRUCTIONS_RETIRED, BACLEARS”. Both PAPI and PAPI native counter names are
accepted.  For example:

.. code-block:: console

    $ survey --counters “UNHALTED_CORE_CYCLES,INSTRUCTIONS_RETIRED, BACLEARS” mpirun -np 32 ./mpiapp

*survey* uses PAPI multiplexing by default. This allows for more PAPI counters to
be collected in a single run, although it may somewhat decrease the accuracy of
the collection process. Some HPC environments may not be configured for this
type of counter collection. To disable multiplexing, include the
flag --disable-papi-multiplex.  For example:

.. code-block:: console

    $ survey --disable-papi-multiplex mpirun -np 32 ./mpiapp
