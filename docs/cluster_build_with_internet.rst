
Building Survey on a cluster with internet access
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The steps for building survey on a non-Cray cluster at a site that
has internet access are outlined below.  With internet access spack can
retrieve the necessary files that are needed to build survey.  On platforms
where internet access is restricted a spack mirror is required.  That is
discussed in a separate web pages found here <...>.

In general the steps to build survey with spack include initializing spack,
modifying configuration files, setting up an externals file for packages that
you don't want spack to build, and finally issuing the spack install command.
spack will automatically create a survey module file that can be used to load
the survey runtime environment.  The setup and build process are described
in the sections below.

1) If you don't have spack already installed then download it:

.. code-block:: console

   cd <spack download path>
   git clone https://github.com/spack/spack.git

Note: If you want to update the spack tree as time goes on,
you can add these commands before you build survey to get the
latest spack changes:

.. code-block:: console

   # Make sure you are using the develop branch of spack
   git checkout -t origin/develop
   # Tie the remote repository to the local one as the upstream repository
   git remote add upstream https://github.com/spack/spack.git
   # update the local repository with what has been checked into
   # the development tree
   git pull upstream develop


2) Setup the path, spack root and module use commands

.. code-block:: console

   setenv SPACK_ROOT <spack download path>/spack
   setenv PATH $SPACK_ROOT/bin:$PATH

3) On a fresh spack download there are some files in the spack repository to change
   for building survey.

   Because the spack default for modules is to module load all of survey's dependencies
   and that is not needed.  We alter the spack modules.yaml file to tell spack
   not to auto load all the survey dependencies.  spack also does not set the default
   module type to be used, so tcl and/or lmod must be added to the "enable[]" structure.

3a) Update $SPACK_ROOT/etc/spack/default/modules.yaml file for autoload: none and "tcl"

Here is an example diff that enables both tcl and lmod::

   diff --git a/etc/spack/defaults/modules.yaml b/etc/spack/defaults/modules.yaml
   index 75ec366117..1ba936ccf5 100644
   --- a/etc/spack/defaults/modules.yaml
   +++ b/etc/spack/defaults/modules.yaml
   @@ -41,15 +41,15 @@ modules:
        tcl:    $spack/share/spack/modules
        lmod:   $spack/share/spack/lmod
       # What type of modules to use ("tcl" and/or "lmod")
   -    enable: []
   +    enable: ['tcl','lmod']
        tcl:
          all:
   -        autoload: direct
   +        autoload: none
        # Default configurations if lmod is enabled
        lmod:
          all:
   -        autoload: direct
   +        autoload: none
          hierarchy:
            - mpi


3b) Update $SPACK_ROOT/etc/spack/default/config.yaml file for short names

On some platforms there are environment variable size restrictions.
If you are on one of those platforms, then this will be of interest
otherwise you may skip this.  The symptom you will see if you are
affected is that when loading the survey module command to run survey
you will get an error about a module environment variable being too
long.
Here is an example diff of a change that will shorten the spack names
to 3 characters::

   diff --git a/etc/spack/defaults/config.yaml b/etc/spack/defaults/config.yaml
   index 532e3db270..734adae66c
   --- a/etc/spack/defaults/config.yaml
   +++ b/etc/spack/defaults/config.yaml
   @@ -19,7 +19,8 @@ config:
      install_tree:
        root: $spack/opt/spack
        projections:
   -      all: "{architecture}/{compiler.name}-{compiler.version}/{name}-{version}-{hash}"
   +      all: '{name}/{version}/{hash:3}'
   +
        # install_tree can include an optional padded length (int or boolean)
        # default is False (do not pad)
        # if padded_length is True, Spack will pad as close to the system max path


4) Setting up the externals file on the build platform

For survey builds, spack allows you to specify already built and
installed packages that your package (in this case survey) needs.
If a package is included in this external list file named: packages.yaml
then that package will not be built by spack.

Typical external packages, for survey, can be MPI related:
openmpi, mpich, mvapich, mpt MPI packages,but other non-mpi
packages can be listed in the packages.yaml and spack
will not build those packages while building survey.
For MPI, papi, and other key components, it is better
to use the system built packages because the system
builds have been tuned to the platforms intricacies
and will generally perform better.

You will want to add the MPI implementation that your
applications will be built with to the packages.yaml
file.  Look for the module file for that MPI implemementation
and then add that to the packages.yaml file as shown in
the example below.  You can also do a module show command
to get the path, prefix in spack terms, to the packages.yaml
file.  Same is true for papi, or other packages that you
don't want spack to build.

The externals file, packages.yaml, contains paths and/or module commands
representing how to find the installations of each component
or library specified.  Arguments are used to indicate what architecture
and the particular compiler the specification applies to.
Here is an example packages.yaml file that was used for survey
on a laboratory platform.  This is an example and its contents
will have to be changed to match your particular MPI or non-MPI
packages you wish to have spack skip the build of the package
but keep track of it and use that package for survey's build.

This file can be stored in $HOME/.spack/linux/ or $SPACK_ROOT/etc/spack/.

.. code-block:: console

 packages:
   all:
      providers:
         pkgconfig: [pkg-config]
   openmpi:
      externals:
      - spec: "openmpi@4.0%gcc@13.2.1 arch=linux-rhel7-broadwell"
        modules:
        - openmpi-gnu/4.0
   mpich:
      externals:
      - spec: "mpich@2018%gcc@13.2.1 arch=linux-rhel7-broadwell"
        modules:
        - intel-mpi/2018
   libtirpc:
      externals:
      - spec: "libtirpc@1.2.6%gcc@13.2.1 arch=linux-rhel7-broadwell"
        prefix: /usr


5) Finding compilers for spack to use in the build

To find the compilers and add them to the compilers.yaml file in ~/.spack/linux
run the spack compiler find command.  If the compiler you desire to use is
loaded by a module file, please load that module before invoking the
"spack compiler find" command.

.. code-block:: console

   spack compiler find


6) Updating the survey package.py file to point to the location of the
   survey tarball.

The survey package.py file will be found at this location:
$SPACK_ROOT/var/spack/repos/builtin/packages/survey/
If you received a special version of that file from one of the
survey developers, place it in the location above and modify it
for your system.

Because survey is not open source, the survey source on gitlab is protected.
When users are given a survey tarball, they must place it in a location on
their build system.  This step shows you how to point the spack survey build
to that tarball from the package.py file that is located in this path:
$SPACK_ROOT/var/spack/repos/builtin/packages/survey. Note the MD5 hash in
the example below needs to be updated with output of the md5sum command.
In this example: md5sum /opt/survey_src/survey-1.1.1.tar.gz
Your location of the file will be different.

So, two changes here: 1) add the version 1.1.1 lines 2) change the MD5 hash.

.. code-block:: console

    version("master", branch="master")
    version('1.1.1', '2b8de62883102d0e76347a8a06ad6b22',
                    url='file:///opt/survey_src/survey-1.1.1.tar.gz')
    version("1.0.9", tag="1.0.9")
    version("1.0.8.1", branch="1.0.8.1")

7) Issuing the spack install command with the proper arguments

Typical install lines for building survey on build platform are of this form:

.. code-block:: console

   spack install --no-cache survey@1.1.1 %gcc@13.2.1 +mpi ^openmpi@4.0

survey has only been built with GNU compilers.  In the example above we are telling
spack to use gcc version 13.2.1 to build survey.  spack must know about gcc@13.2.1,
therefore the "spack compiler find" command needed to find that particular version.
The "+mpi" indicates to spack that the survey build will be attempting to build
MPI collectors.  The "^openmpi@4.0" tells spack and the survey build that this
build wants to use openMPI 4.0 to build those MPI data collectors.
The version specification above, such as survey@1.1.1 must match a tarball verion
specified in the survey package.py file.  The tarball file that matches the survey
version string above must be: survey-1.1.1.tar.gz.
The "--no-cache" spack install option tells spack not to look in its cache to find
the source.  This is too avoid using an older version, if any is present, of the
source for survey.


8) A note about survey and MPI implementations

spack has a rule that inforces one spack build per MPI implementation type.
survey uses the MPI implementation's data structures when
collecting the MPI statistics from an application code.  That means
that the survey built with a particular MPI implementation must be
used on an application built with the same MPI implementation.
If survey built for openmpi is used on an application built with
mpich there may be discrepencies in the MPI data statistics reported
by survey.
Therefore, if you are using survey on applications built with different
MPI implementations, you will have to build a separate version of
survey for each unique MPI implementation used in your applications.
In other words if you have applications built with openmpi and mpich,
then you will have to build survey twice, once for openmpi and once
for mpich.

9) Finding and using the spack created survey module file

The spack TCL module file that is created by spack based on
the information/settings in the survey/package.py
file and is indexed by the value of the command: spack arch.
The modules files can be found in the spack tree at this location
for the build platform:

.. code-block:: console

   $SPACK_ROOT/share/spack/modules/<output of spack arch>

The next steps are to load the survey module file using these steps:

.. code-block:: console

   module use $SPACK_ROOT/share/spack/modules/<output of spack arch>
   module load survey/<spack created survey module filename>

Here is a real life module location and naming example:

.. code-block:: console

   cd /p/workdir/user99/spack/share/spack/modules/linux-sles15-zen2/survey/
   ls -lastr
   total 24
   12 drwxr-xr-x 115 user99 3414L200 12288 Dec 31 13:27 ..
   08 -rwxr-xr-x   1 user99 3414L200  5464 Dec 31 13:28 1.1.0.release-gcc-12.2.0-lfqb5fy
   04 drwxr-xr-x   2 user99 3414L200  4096 Dec 31 13:28 .

In this case to access survey via modules one would execute these commands:

.. code-block:: console

   module use /p/workdir/user99/spack/share/spack/modules/linux-sles15-zen2
   module load survey/1.1.0.release-gcc-12.2.0-lfqb5fy


