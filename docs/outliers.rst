Outliers
--------

An outlier is a measurement that is, statistically speaking, significantly
different than the other measurements in the run. Survey examines several different
measurements, determines, and identifies any results that are outliers. This
examination is not performed for all measurements because of the extra
computation that is required for this calculation. By default, this list of
measurements is:

* total_time_seconds
* omp_compute
* total_mpi_time_seconds
* computation_intensity

This list can be modified by editing the survey configuration file (config.yaml, by default),
and updating the outlier_metrics list.

The inter quartile range method is used to determine outlier values. In this
approach:

#. All data points for the measurements are sorted and the Q1 and Q3 (first and third quartiles) are identified.

#. The Interquartile Range (IQR) is calculated: Q3 - Q1

#. The Lower Fence is calculated: Q1 - 3 * (IQR)

#. The Upper Fence is calculated: Q3 + 3 * (IQR)

#. Any data point that is less than the lower fence or greater than the upper fence is considered an outlier and included on the json report.

