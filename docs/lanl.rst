*survey* documentation for LANL environment
===========================================

Darwin
------

*survey* is built on most Darwin partitions. To set the module use path once
you get a partition:

source /usr/projects/trenza/survey_mod.sh

This will set the path for the appropriate architecture based on the partition.

module avail will show the survey modules available.

Then load the survey module for the mpi version that you are using.

support:
dmont@trenzasynergy.com
