Detailed Instructions for mirror usage on build platform
--------------------------------------------------------

Steps on the mirror usage (build) platform that has no internet access
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If one can't git clone spack on the build platform, it will need to be
tarred up and transferred from the mirror creation platform.

The steps listed here are for either case.
The first step on the build platform is to unpack the *survey* related
spack mirror and add the mirror to the build platform version of spack.
If there is no spack installation installed on the build platform then
use similar steps that were used on the mirror creation platform to
install spack.  Steps are shown below for completeness.

.. code-block:: console

    git clone https://github.com/spack/spack.git
    git checkout -t origin/develop
    git remote add upstream https://github.com/spack/spack.git
    git pull upstream develop

Next, unpack the *survey* related spack source mirror tarball into
a directory of choice and issue the spack mirror add command.
The spack mirror add command will tell spack to look in the
specified directory for the source tarballs during the *survey*
build.

Based on your subscription or trial agreement, obtain the *survey* tarball and
spack package.py file from a Trenza representative by emailing:

- Dave Montoya          dmont@trenzasynergy.com
- Jim Galarowicz        jeg@trenzasynergy.com

.. code-block:: console

    date=02-20-2022
    tar -zxvf survey-mirror-$date.tar.gz
    spack mirror add spack-linux-mirror /user/jgalaro/survey-mirror-$date.tar

The *survey* build is now ready to go.
An example *survey* related spack install command is illustrated below.

.. code-block:: console

    spack install survey@<survey version> %gcc@7.2.0 +mpi ^openmpi

To use the build one must do a module load of the *survey* module file
that was created by spack.  After that *survey* can be executed.  An
example *survey* run command is shown below.

.. code-block:: console

    module use $SPACK_ROOT/share/spack/modules/$(spack arch)
    ls -lastr $SPACK_ROOT/share/spack/modules/$(spack arch)/survey*
    module load <survey module file name found above>

    survey mpirun -np 256 ./my_mpi_application
