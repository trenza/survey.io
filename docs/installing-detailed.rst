Detailed Instructions for Installing *survey* with spack
========================================================

Installation process: Quick Start Guide
---------------------------------------

Based on your subscription or trial agreement, obtain the *survey* tarball and
spack package.py file from a Trenza representative by emailing:

- Dave Montoya          dmont@trenzasynergy.com
- Jim Galarowicz        jeg@trenzasynergy.com

After obtaining the *survey* source tarball, unpack the tarball
and change directories into the top-level spack directory.

Then, initialize the spack build environment: initialization
and compiler recognition.  If there are compiler modules for the
version of the compiler you want to use in spack, then load the
module and do "spack compiler find" so spack finds that compiler
later when you want to use it.

.. code-block:: console

    export SPACK_ROOT=<where your spack is>/spack
    source $SPACK_ROOT/share/spack/setup-env.sh
    spack compiler find

*survey* is now officially in the spack list of applications, therefore
one can inspect the survey package.py file by editing the survey package.py
file that is found in the spack repos builtin packages directory.

.. code-block:: console

    <editor of choice> $SPACK_ROOT/var/spack/repos/builtin/packages/survey/package.py

Another step that is optional but highly recommended is to use an
installed version of a MPI implementation in the spack build process.
To tell spack you want to use an external (already installed) version
of a package, like MPI, one needs to create a packages.yaml file whose
contents point to the external package.  The packages.yaml file can be
placed in the user's $HOME/.spack/linux directory.  That is where spack
stores a number of files.

.. note::

    Using the system installation of MPI is very important on systems
    using job schedulers because on those systems the MPI implementation
    is likely integrated with the job scheduler and vice versa.  Not
    using the system installed MPI implementation may cause *survey* to
    fail because of MPI and job scheduler mismatch.  It is highly recommended
    to use a GNU/GCC version of the MPI implmentation if possible.  survey
    is compiled with GNU/GCC compilers by default.

The externals package.yaml example below shows an existing installation
of openmpi-3.1.4 that can be found in the /opt/openmpi-3.1.4 directory.
Any other dependency that is specified in the externals package.yaml
file will be used by spack instead of spack building the dependency.

.. code-block:: console

    packages:
       openmpi:
          externals:
          - spec: "openmpi@3.1.4%gcc@9.3.0 arch=linux-ubuntu20.04-skylake"
            prefix: /opt/openmpi-3.1.4

One can also use the module form of the externals definition as listed below.

.. code-block:: console

    packages:
       openmpi:
          externals:
          - spec: "openmpi@3.1.4%gcc@9.3.0 arch=linux-ubuntu20.04-skylake"
            modules:
            - openmpi/3.1.4

At this point we are ready to build *survey* using the spack build manager with
one of the following example spack build commands.  This command will build
*survey* with no MPI capabilities.

.. code-block:: console

    spack install survey


This spack build command will build *survey* with OpenMPI MPI capabilities.
The spack build will use the previously installed /opt/openmpi-3.1.4
version of OpenMPI in building this version of *survey*.  The *+mpi* option
on the spack build command is a survey variant.  It tells the survey spack build
to build the survey MPI data collectors with the MPI specified to or found by spack.
See this `section <https://spack.readthedocs.io/en/latest/packaging_guide.html?highlight=variants#variants>`_:
in the spack documentation for more details about spack variants.  The *^openmpi*
option on the spack command line tells spack that this build should use openmpi for the
MPI implementation used to build the survey MPI data collectors.  If there are multiple
versions of openmpi specified in the externals package.yaml file then one can specify
a specific version of the openmpi implementation by using ^openmpi@4.1.2, where 4.1.2
is the version value of one of the openmpi entries in the externals package.yaml file.

.. code-block:: console

    spack install survey +mpi ^openmpi

This spack build command will build *survey* with MPICH MPI using gcc-7.2.0
The spack build will have to build a version of MPICH MPI when building
this version of *survey* because there was no entry in the packages.yaml file.
If one was added then spack would not build MPICH while building *survey*.

.. code-block:: console

    spack install survey %gcc@7.2.0 +mpi ^mpich


To use *survey*, one can do a module load of the *survey* tool and if MPI was
chosen, a module load of the MPI version should also be loaded.  spack module
files are built automatically when the spack build takes place they are found
in a modules sub-directory based on the platform architecture.

.. code-block:: console

    module use $SPACK_ROOT/share/spack/modules/$(spack arch)
    ls -lastr $SPACK_ROOT/share/spack/modules/$(spack arch)/survey*
    module load <survey module file name found above>

To run *survey* place the *survey* name in front of how you normally run your
application command.  See the simple run example below and other examples in
the running *survey* section:

.. toctree::
   :maxdepth: 2

   running.rst


.. code-block:: console

    survey mpirun -np 256 ./my_mpi_application


Installation process: Using spack mirrors
-----------------------------------------

On systems where internet access is restricted the spack build manager provides
a mirror feature that allows the spack user to point spack to a directory containing
the source that would have been accessed remotely if the build system had internet
access.  The user must creating this directory by using spack on platform that does
have internet access, tarring it up and moving the source mirror to the build platform
that does not have internet access.  To create the mirror one uses the
"spack mirror create ..." command and to use it on the build platform one uses the
"spack mirror add ..." command.

Note: The spack version and update level on the mirror creation platform
and the spack version on the mirror use (build) plaform must be the same.
If they are not, there is a likely chance that the versions of some packages
will not match and the build will fail because the source files in the source
mirror will not be the same as that of the spack repository itself.

A detailed example is illustrated below.

Steps on the mirror creation platform with internet access
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For illustration purposes this example uses the latest development
sources for spack and does a git pull to obtain the very latest
changes.  One could just do a git clone of spack and use that on
both the mirror creation platform and the mirror usage platform.

.. note::

    Again, note that the spack versions and update level need to be
    the same.  Here we clone spack and git pull the latest updates.

.. code-block:: console

    git clone https://github.com/spack/spack.git
    git checkout -t origin/develop
    git remote add upstream https://github.com/spack/spack.git
    git pull upstream develop

Based on your subscription or trial agreement, obtain the *survey* tarball and
spack package.py file from a Trenza representative by emailing:

- Dave Montoya          dmont@trenzasynergy.com
- Jim Galarowicz        jeg@trenzasynergy.com

The next step is to edit the spack *survey* package file from the spack
packages directory in order to make sure the correct version of the *survey*
source tarball is included in the spack source mirror of the packages needed
in the spack *survey* build.  spack will look at the survey package.py file
in order to process the survey version specified on the spack mirror create
command (see below).

.. code-block:: console

    <editor of choice> $SPACK_ROOT/var/spack/repos/builtin/packages/survey/package.py

.. note::

    At this point please check that your $HOME/.spack/linux/packages.yaml
    file doesn't contain any packages that you will need to build on the
    mirror use (build) platform.  If it does, the "spack mirror creation ..."
    command will not create a source tarball for those packages causing
    the spack *survey* build to fail on the mirror use (build) platform.
    Also, adjust the spack *survey* package.py file to point to the *survey*
    tarball that you obtained from the Trenza representative.  One can use
    the version file command to point to the *survey* tarball.  For example:
.. code-block:: console

   version('1.1.0', 'bf5e354772f0cfa7e08d781caaae26ce',
           url='file:///home/jgalarowicz/survey-1.1.0.tar.gz')

.. note::

    Please adjust the path to point to the *survey* tarball.
    Then the spack mirror creation command will find the *survey* tarball and
    include it in the spack mirror.

The next steps are done on the mirror usage (build) platform and are explained in
the next section.  This completes what needs to be done to create the spack
*survey* related source mirror.

.. code-block:: console

    date=01-20-2025
    spack mirror create -d /home/jgalarowicz/survey-mirror-$date -D survey@<survey version> +mpi ^<mpi implementation>
    tar -cvf survey-mirror-$date.tar /home/jgalarowicz/survey-mirror-$date
    gzip survey-mirror-$date.tar
    scp survey-mirror-$date.tar.gz to mirror usage (build) platform



Steps on the mirror usage (build) platform that has no internet access
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If one can't git clone spack on the build platform, it will need to be
tarred up and transferred from the mirror creation platform.

The steps listed here are for either case.
The first step on the build platform is to unpack the *survey* related
spack mirror and add the mirror to the build platform version of spack.
If there is no spack installation installed on the build platform then
use similar steps that were used on the mirror creation platform to
install spack.  Steps are shown below for completeness.

.. code-block:: console

    git clone https://github.com/spack/spack.git
    git checkout -t origin/develop
    git remote add upstream https://github.com/spack/spack.git
    git pull upstream develop

Next, unpack the *survey* related spack source mirror tarball into
a directory of choice and issue the spack mirror add command.
The spack mirror add command will tell spack to look in the
specified directory for the source tarballs during the *survey*
build.

.. code-block:: console

    date=01-20-2025
    tar -zxvf survey-mirror-$date.tar.gz
    spack mirror add spack-linux-mirror /user/jgalaro/survey-mirror-$date.tar

The *survey* build is now ready to go.
An example *survey* related spack install command is illustrated below.

.. code-block:: console

    spack install survey@<survey version> %gcc@7.2.0 +mpi ^openmpi

To use the build one must do a module load of the *survey* module file
that was created by spack.  After that *survey* can be executed.  An
example *survey* run command is shown below.

.. code-block:: console

    module use $SPACK_ROOT/share/spack/modules/$(spack arch)
    ls -lastr $SPACK_ROOT/share/spack/modules/$(spack arch)/survey*
    module load <survey module file name found above>

    survey mpirun -np 256 ./my_mpi_application



Installation process: For a Cray platform
-----------------------------------------

For Cray builds the steps are the same except one must specify
the os we are targeting for also change the compiler to match
the one you are building for and set the Cray environment to PrgEnv-gnu.

.. code-block:: console

    module swap PrgEnv-intel PrgEnv-gnu   # may be PrgEnv-cray PrgEnv-pgi
    spack install survey %gcc@8.3.0 +mpi ^mpich os=sles15

.. note::

    spack externals specification note for building survey on a Cray platform:
      If setting up the externals package file for Cray mpich, note that
      there is a bug/issue in spack (at this time: 10/29/21) that requires
      that both the *modules* and *prefix* keywords be specified in the externals
      definition file: packages.yaml.  The example below shows both keywords
      specified in defining the external path to Cray mpich.

.. code-block:: console

    packages:
      mpich:
        buildable: false
        externals:
        - spec: mpich@7.7.16%gcc@8.3.0 arch=cray-sles15-broadwell
          prefix: /opt/cray/pe/mpt/7.7.16/gni/mpich-gnu/7.1
          modules:
          - cray-mpich/7.7.16


On some Cray platforms "aprun" is the preferred job launch command, so
a *survey* command would use that command.  In that case, an example usage
would be as shown below.  On many other Cray platforms, srun is used instead
of aprun. Check the platform users guide for what is standard on your Cray platform.

.. code-block:: console

    survey aprun -n 88 ./nbody-gnu
