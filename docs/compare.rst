Survey Compare
--------------
To generate a compare report, use: survey_compare <prefix>,<prefix>,<etc> where <prefix> is a location of the prefix
of a survey run. Two or more prefixes can be specified, delimited by commas.

Survey compare generates two html files comparing the results of two or more specified *Survey* runs.

* survey_compare.html
* survey_compare_metadata.html

survey_compare.html contains a table containing the measurement results of the specified runs. The rows
consists of each min, max, average measurement result. The columns consist of each specified run. This allows you to
easily compare the results of multiple runs side-by-side.

survey_compare_metadata.html is similar to the measurement report table except that only the first column contains
all the metadata results. In the subsequent columns, the results are only shows if they are different than the
results in the first column. This is done because often the metadata will not vary from run to run. This allows you
to spot quickly where any changes were made between runs.

By default, a directory is generated for these files called "survey_compare_html-X". To specify a different location,
you can use the --output option.

An example usage of survey_compare follows:

.. code-block:: console

   $ survey_compare amg2013-survey,amg2013-survey-0,amg2013-survey-1
   [survey-extractor] Created measurement compare report: /home/fred/performance_tests/AMG2013/test/survey_compare_html/survey_compare.html
   [survey-extractor] Created metadata compare report: /home/fred/performance_tests/AMG2013/test/survey_compare_html/survey_compare_metadata.html

An image of the survey_compare.html follows.

.. image:: files/amg2013_survey_compare.png
  :scale: 35%
  :align: center

These are the available parameters of the *survey_compare* command:

* <target>: A comma-delimited list of *survey* report files such as: myapp-survey-1,myapp-survey-2
* --project <project-name>: Specify a project file to use one of its configuration files.
* --config <extractor config file>: Specify a config file from the default or specified project file.
* --output <file>: Specify a location to output the generated html report files.
