*survey* output
===============

The *survey* tool generates various outputs that can be controlled via run
options.   survey produces a summary report that is streamed to standard
out (stdout).  survey also produces a comma separated list (csv) file
containing the application performance data.  Another output is a comprensive
combination of the performance data similar to the stdout information and
also includes job, system, and application metadata.  The raw data files
per thread that are used to create the above files is also available to the
user.

Multiple runs of the same app will increment the number (#) in the
*survey* file names used to label the csv, json, and raw data files
and directories. The default file structure is as follow:

json file
---------

<app>-survey-csvdata-#-report.json
This file contains the job and system metadata as well as the summary
performance metrics that include: overall time, DMEM, MEMALLOC, MEMFREE, PAPI,
IO, and MPI

.. image:: files/output_json1.png
  :scale: 35%
  :align: left
.. image:: files/output_json2.png
  :scale: 35%
.. image:: files/output_json3.png
  :scale: 35%
.. image:: files/output_json4.png
  :scale: 35%
.. image:: files/output_json5.png
  :scale: 35%

csv file
--------

<app>-survey-csvdata-#-report.csv
This summary file contains performance metrics that include: overall time,
DMEM, MEMALLOC, MEMFREE, PAPI, IO, and MPI

.. image:: files/output_csv.png
  :scale: 40%
  :align: center
