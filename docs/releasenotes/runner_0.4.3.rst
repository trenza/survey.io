HPC GitLab-Runner Release 0.4.3
===============================

* `Release Date` 09/06/2019

Security Fixes
--------------

* The GitLab server provides variables to the runner as part of a request to
  the API each job calls. These variables are in increasing
  order of precedence such as the runner will fetch the first instance of a
  key (see
  `priority of environment variables <https://docs.gitlab.com/ee/ci/variables/README.html#priority-of-environment-variables>`_
  ). There are two issues, the first and biggest issue is users can override
  even the predefined variables. In addition the order provided by the server
  in-specific cases can be incorrect. As such we’ve decided to create a list
  of eight protected predefined variables that can be leveraged by SetUID /
  Federation and if a user attempts to set (deployment, YAML, pipeline, group,
  or trigger level variables) the job will fail.

 - User’s will receive an error message explaining why the job failed and it
   occurs even before SetUID, as such it cannot be bypassed.

 - There is also an upstream issue to better address this with the help of
   GitLab with a fix to the server and customer executor. This would offer a
   more robust solution; however, the fix that is now in place with the runner
   will always insure that undesirable behavior is accounted for and prevented.


General Changes
---------------

* The after_script section no longer runs as part of job submitted to the
  scheduler. It is now treated as it’s stage and will execute via the node
  on which the GitLab-Runner service is located (e.g. the scheduling/login
  node).

 - Keep in mind that even though the folder structure will remain the same
   when the after_script runs it will be executed in a new shell, so manually
   managed environmental variables and modules will need to be accounted for.

 - Note: there had been interested in supporting a configurable option;
   however, it will provide a more stable runner if this is simply the new
   behavior for the batch executor.

* Job scripts created by a runner in order to be submitted to the scheduler
  have had their names changed to appear as:
  `<CI_JOB_ID>_<uniqueIdentifier>.sh`

* There will no longer be additional scripts created for each sub-stage within
  a job (e.g. `get_source`, `download_artifact`, etc...). This should help
  decrease potential clutter and unnecessary access of the file system.

* A failed job in the batch executor now correctly differentiates between a
  system error and a build error (e.g. an issue with the script). This means
  actions such as uploading an artifact on failure will now function
  correctly.

Admin Changes
-------------

* Optional support had been added to specify a target `batch_system` via the
  `config.toml`. If not specified the runner will revert to attempting to
  detect the appropriate scheduler as it has in earlier versions.

 - .. code-block:: toml

        [[runners]]
            name = "example-runner"
            executor = "batch"
            batch_system = "slurm"

 - Supported schedulers and their associated configuration currently include:
    * “cobalt” → Cobalt (using ``qsub``)
    * “slurm” → Slurm (using ``sbatch``)
    * “lsf” → LSF (using ``bsub -I``)

* Each CI job consists of a number of sub-stages (e.g. `get_source`,
  `download_artifact`, etc...), each executing a GitLab generated script.
  Though the script is still generated the Batch executor no longer creates a
  file in the file system instead passes it to the user’s shell (bash --login
  ...) via `stdin`.

  - This is the same behavior as the shell executor.
    Any issues should be reported right away to the CI team and should
    only be encountered when you first test the runner deployment.

* The Runner “concurrency issue” should now be addressed.
  This bug was caused by a race condition in the batch
  executor that appeared to have only been encountered by some sites.

  - This bug manifested as a git error such as “error: could not lock
    config file .git/config: File exists” or interactions in the file system
    with folder that already exist.

* Registering new non-federated runners will no longer incorrectly
  display the IAL/AAL settings in the ``config.toml``.

* Interactive registration for federated runners has been updated to provide
  validation of IAL/AAL ranges and clarified instructions.

* User’s canceling jobs will no longer led to unnecessary stdout by the
  GitLab-Runner process and the associated runner logs have been clarified.
