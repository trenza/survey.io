Top-Down Microarchitecture Analysis in *survey*
===============================================

Top-Down analysis is included as an optional calculated
measurement in the *survey* profile tool. Most tools that
perform this type of analysis do so on single-threaded
applications or single-threaded parts of an application.
Top-down analysis in the *survey* tool can be performed
on MP and MPI applications. The minimum, maximum, and
average values of each calculated measurements are
included in the *survey* results.

What is Top-Down Microarchitecture (TMA) Analysis?
--------------------------------------------------
TMA is a method of measuring how efficiently an application uses the
resources of an Intel-based CPU. TMA is designed to help the software
developer identify CPU-based bottlenecks in order to optimize
performance during the development process. It was first
developed by Ahmad Yasin at Intel Corporation.[#]_

The CPU performance is broadly divided into four main categories:

* Frontend Bound: When the frontend of the CPU is not supplying
  enough instructions to the backend of the CPU.
* Backend Bound: When the backend of the CPU is not able to
  accept microoperations (uops) due to a lack of resources.
* Bad Speculation: When bad speculation on the CPU leads to wasted
  resources.
* Retiring: When uops are correctly retired. This is generally a
  desired occurance, but a high retiring value does not necessarily
  indicate that optimizations to the application are not possible.

At the end of the run, the uops of the application are divided
into these four broad categories for each rank and each
thread, and the minimum, maximum, and the arithmetic mean are
calculated and displayed.

More runs can be made in order to further drill down into each
of the four categories as described below.

Frontend Bound
^^^^^^^^^^^^^^
When an application is frontend bound, the frontend of the CPU
is not supplying uops to the backend of the processor.
Frontend Bound can further be divided into two sub-categories:

* Frontend Latency Bound: When the backend is not being supplied
  with uops because of issues such as ITLB misses, instruction
  cache misses, or branch re-steers, which occur with branch
  mispredictions and are closely related to the Bad Speculation
  category.

* Frontend Bandwidth Bound: When the frontend is not supplying
  uops to the backend because of inefficiencies in the
  instruction decoders or other issues not related to the
  frontend latencies.

Backend Bound
^^^^^^^^^^^^^
An application is backend bound when it cannot accept uops due
a lack of resources. This category is further divided into
the sub-categories of memory bound and core bound.

* Memory Bound: When there is an execution stall due to a
  load missing in various parts of the memory hierarchy.

* Core Bound: When there are backend execution stalls that
  are not related to memory such as a long division or
  dependent arithmetic operations. Such issues can sometimes
  be decreased with better compiler operation scheduling.

Bad Speculation
^^^^^^^^^^^^^^^
Speculation are steps the CPU takes that may end up not
actually being needed. Bad speculation is divided into two
sub-categories:

* Branch Mispredicts: Resource loss due to bad branch
  predictions.

* Machine Clears: Events such as memory misordering, self-
  modifying code, and loads to illegal addresses require
  the entire CPU pipeline to be cleared. Any uops taken
  that are required to be cleared due to such a clear is
  part of this sub-category.

Retiring
^^^^^^^^
Uops that are retired represent useful work that has
been completed. This is generally a positive condition,
although it doesn't necessarily mean that the code
cannot be further optimized. There are two sub-categories:

* Microcode Sequencer (MS) ROM: These are typically CISC
  (Complex Instruction Set Computers) instructions not fully
  decoded by the default decoders and then fetched by the
  MS ROM before being retired.

* Base: The fraction of retired instructions not fetched
  by the MS ROM. That is, normal uops that are typically
  executed within one clock cycle.

*survey* implementation of TMA
------------------------------
Each TMA calculation requires a number of counters to be
measured while the application runs. Only a limited number
of counters can be measured at a time before the
measurement has a meaningful impact on the application's
performance. For this reason, TMA has been divided into
sections corresponding to the TMA categories:

--top-down
^^^^^^^^^^
This will measure all four top-level categories of TMA
(frontend, backend, bad speculation, and retiring).

--top=down-fe
^^^^^^^^^^^^^
This will measure the frontend category, including
measurements within its subcategories. This is the
percentage of uops when the frontend of the CPU
is delayed in supplying operations to the backend.

--top-down-be
^^^^^^^^^^^^^
This option measures the backend category, which is
the percentage of operations that cannot be accepted
by the backend due to a lack of resouces. There
are a number of different measurements in this
category that are further divided up into memory-bound
and core-bound.

--top-down-mem-bound
^^^^^^^^^^^^^^^^^^^^
This measurement is part of the backend category. It
represents the fraction of uops that are stalled due
to loads or stores. This measurement is further broken
up into sub-categories indicating the level of the
memory stack where the stalls occurred.

--top-down-core-bound
^^^^^^^^^^^^^^^^^^^^^
This is the fraction of uops that were stalled by the
backend because of reasons other than memory issues.

--top-down-bs
^^^^^^^^^^^^^
Measurements in this category represent operations that
are stalled due to bad speculation.

--top-down-ret
^^^^^^^^^^^^^^
This category represents the fraction of uops that are
successfully retired.

Additional Notes
----------------
Each TMA measurement represents a number of different
counters measured during the execution of the application.
Only the counters needed for a particular set of
measurements are included.  However, there is some overlap
in counters used in various measurements. After the run
is completed, if all the counters were measured for a
particular TMA measurement, then it is included in the
output, even if that particular measurement isn't part of
the *survey* option that was specified.

Each TMA measurement is calculated for each thread of
execution. The minimum and maximum values for each
measurement represent the minimum and maximum calculated
values across all requested threads/ranks
(default is ALL nodes).  The average represent the
average calculated values across all requested
threads/ranks.

.. [#] A. Yasin, "A Top-Down method for performance analysis and counters
   architecture," 2014 IEEE International Symposium on Performance Analysis of
   Systems and Software (ISPASS), Monterey, CA, 2014, pp. 35-44, doi:
   10.1109/ISPASS.2014.6844459.
