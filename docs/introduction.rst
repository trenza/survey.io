.. _survey-introduction:

Overview
========

*survey*
--------

What is *survey* all about?
^^^^^^^^^^^^^^^^^^^^^^^^^^^

*survey* is a performance measurement tool for applications
running in environments from a high performance computing (HPC)
center to a user laptop. In addition to application metrics,
survey also also collects metadata about the system
and runtime environment.  *survey* was designed to be lightweight
enough to be used regularly as part of a continuous integration or testing
environment while still collecting and displaying a wide range of performance
metrics to help diagnose issues with changing codes or changing environments.

Why is it needed?
^^^^^^^^^^^^^^^^^

Most HPC performance analysis tools produce highly detailed specialized information,
require significant resources to run, interfere with the overall performance of the application,
and require a user with analytic skills to interpret the results. For these reasons,
these tools are only used occasionally in problem cases (similar to debuggers) and are not
designed to be used routinely with normal use of the application. However, performance data
can provide extremely useful information in a continuous integration (CI) development approach,
allowing the user to assess the impact of regular changes to the application or software
environment.

*survey* collects multiple metrics with low resource overhead, allowing it to be integrated into
the normal use of the application. This capability provides insight into an application's
interaction with the compute environment and gives the user a summary view of performance
collected at a process level. *survey* summarizes the results in a flexible format (csv, json)
for integration into other analysis tools and databases.

How should it be used?
^^^^^^^^^^^^^^^^^^^^^^

We have identified use cases below where it would be beneficial.  *survey's*
capabilities allow it to provide metrics that can identify problem areas that
are occurring during the development process, cross architecture issues, and
issues when other aspects of the system environment change. It provides a key
for further forensic work that can be done by heavier tools and debuggers.

Survey Introduction Videos
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. raw:: html

    <a href="https://www.youtube.com/watch?v=9CtE3WMhFno&t=0s">Introduction to survey</a></br>


    <a href="https://www.youtube.com/watch?v=iaWK-DFM8kA&t=0s">Make Your Runs More Efficient with survey</a>

|

Use Cases
---------

Software Development
^^^^^^^^^^^^^^^^^^^^

Individual developers and/or developer team can use survey to monitor performance
as they develop changes to their application or project. *survey's* performance
data files are output in both csv and json format. This data can be uploaded
to other analysis tools for further metric analysis. *survey* also provides
options for Intel top-down analysis. *survey* could also be
easily integrated into a project's regression testing framework to provide
performance tracking.

Continuous Integration
^^^^^^^^^^^^^^^^^^^^^^

The *survey* performance tool can be integrated into a CI pipeline such as GitLab CI.
This would allow a pipeline to not only report back that it ran successfully but
also report back performance metrics and metadata for that run.

System Testing
^^^^^^^^^^^^^^

Since it is easily integrated and lightweight, the *survey* tool could be
integrated into machine acceptance tests, post DST, and other testing where
performance metrics would be beneficial for comparison to identify system or
software deployment issues.

Monitoring System Integration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Similar to the testing use case, integration into a system monitoring
infrastructure would provide a view of metrics from the applications
perspective. This would be ideal when integrated with other tools that collect
data such as LDMS, resource managers (such as Slurm), and others.

License Model
-------------

*survey* is available with flexible subscription licensing based on the use case
and scale of the system environment. This subscription pricing gives us the resources
to continue to develop new features and provide good support to our users.
Site subscriptions can be established based on the requirements and needs of the site. We will
also provide trial licenses for assessment.
