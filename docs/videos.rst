
Educational Videos
==================

What are *survey* educational videos?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

*survey* videos are developed to both describe how survey can be used
to provide information about application performance and support preperation
studies to optimize resource use and decrease time to solution. They also
provide knowledge and reference to tools and attributes that impact building and
running of your application such as MPI, OpenMP, affinity, etc.

Structure
^^^^^^^^^

We have loosely categorized videos by topic areas. We have tried to provide
overiew videos as well as short focued topic videos. They are continuously being
updated and added to. Feel free to provide feedback and recommendations for others.


*survey* - Overview / Run Options
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. raw:: html

    <a href="https://www.youtube.com/watch?v=9CtE3WMhFno&t=0s">Introduction to survey</a></br>

    <a href="https://www.youtube.com/watch?v=OLs6RUq0J5w&t=0s">Survey Prefix</a></br>

    <a href="https://www.youtube.com/watch?v=dsWejdQ7BBM">Survey Text Report</a></br></br>



*survey* - Run Resource Optimization
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. raw:: html

    <a href="https://www.youtube.com/watch?v=iaWK-DFM8kA&t=0s">Make Your Runs More Efficient with survey</a></br>


    <a href="https://youtu.be/FRx1Th1KGKc">How Bad Affinity Can Kill Your Performance</a></br></br>


*survey* - MPI series
^^^^^^^^^^^^^^^^^^^^^

.. raw:: html

    <a href="https://www.youtube.com/watch?v=5YNoFPWNxyY&list=PLCfYbPcs2iU8KD4X9NvpkaGSMGw0Q_Skl&index=9">A Beginner's Guide to Testing MPI Applications with Trenza Survey</a></br>

    <a href="https://www.youtube.com/watch?v=kCAeB28jtg0&list=PLCfYbPcs2iU8KD4X9NvpkaGSMGw0Q_Skl&index=10">What every HPC User need to know about MPI Point to Point Operations</a></br></br>


*survey* - Study Management
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. raw:: html

    <a href="https://www.youtube.com/watch?v=VB-sOSKs6dk&t=0s">Survey Log</a></br>

    <a href="https://www.youtube.com/watch?v=qgfMSQjGoIw&t=0s">Survey Metadata</a></br>

    <a href="https://www.youtube.com/watch?v=xfHrVQn1cBg&t=0s">Survey Project</a></br></br>
