Detailed Instructions spack mirror creation on platform with internet access
============================================================================

Installation process: Using spack mirrors
-----------------------------------------

On systems where internet access is restricted the spack build manager provides
a mirror feature that allows the spack user to point spack to a directory containing
the source that would have been accessed remotely if the build system had internet
access.  The user must creating this directory by using spack on platform that does
have internet access, tarring it up and moving the source mirror to the build platform
that does not have internet access.  To create the mirror one uses the
"spack mirror create ..." command and to use it on the build platform one uses the
"spack mirror add ..." command.

Note: The spack version and update level on the mirror creation platform
and the spack version on the mirror use (build) plaform must be the same.
If they are not, there is a likely chance that the versions of some packages
will not match and the build will fail because the source files in the source
mirror will not be the same as that of the spack repository itself.

A detailed example is illustrated below.

Steps on the mirror creation platform with internet access
----------------------------------------------------------

For illustration purposes this example uses the latest development
sources for spack and does a git pull to obtain the very latest
changes.  One could just do a git clone of spack and use that on
both the mirror creation platform and the mirror usage platform.

.. note::

    Again, note that the spack versions and update level need to be
    the same.  Here we clone spack and git pull the latest updates.

.. code-block:: console

    git clone https://github.com/spack/spack.git
    git checkout -t origin/develop
    git remote add upstream https://github.com/spack/spack.git
    git pull upstream develop

    Set up your SPACK_ROOT variable:

    .. code-block:: console

       export SPACK_ROOT=<spack install path>

Based on your subscription or trial agreement, obtain the *survey* tarball and
spack package.py file from a Trenza representative by emailing:

- Dave Montoya          dmont@trenzasynergy.com
- Jim Galarowicz        jeg@trenzasynergy.com

The next step is to edit the spack *survey* package file from the spack
packages directory in order to make sure the correct version of the *survey*
source tarball is included in the spack source mirror of the packages needed
in the spack *survey* build.  spack will look at the survey package.py file
in order to process the survey version specified on the spack mirror create
command (see below).

.. code-block:: console

    <editor of choice> $SPACK_ROOT/var/spack/repos/builtin/packages/survey/package.py

.. note::

    At this point please check that your $HOME/.spack/linux/packages.yaml
    file doesn't contain any packages that you will need to build on the
    mirror use (build) platform.  If it does, the "spack mirror creation ..."
    command will not create a source tarball for those packages causing
    the spack *survey* build to fail on the mirror use (build) platform.
    Also, adjust the spack *survey* package.py file to point to the *survey*
    tarball that you obtained from the Trenza representative.  One can use
    the version file command to point to the *survey* tarball.  For example:
.. code-block:: console

   version('1.0.0', 'bf5e354772f0cfa7e08d781caaae26ce',
           url='file:///home/jgalarowicz/survey-1.0.5.tar.gz')

.. note::

    Please adjust the path to point to the *survey* tarball.
    Then the spack mirror creation command will find the *survey* tarball and
    include it in the spack mirror.

The next steps are done on the mirror usage (build) platform and are explained in
the next section.  This completes what needs to be done to create the spack
*survey* related source mirror.

.. code-block:: console

    date=02-20-2022
    spack mirror create -d /home/jgalarowicz/survey-mirror-$date -D survey@<survey version> +mpi ^<mpi implementation>
    tar -cvf survey-mirror-$date.tar /home/jgalarowicz/survey-mirror-$date
    gzip survey-mirror-$date.tar
    scp survey-mirror-$date.tar.gz to mirror usage (build) platform
